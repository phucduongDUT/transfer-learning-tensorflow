from numpy import loadtxt
from keras.models import load_model
import tensorflow as tf
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np

model = load_model('best-model.h5')
# model.summary()

def load_image(img_path, show=False):
    img = image.load_img(img_path, target_size=(150, 150))
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])
        plt.axis('off')
        plt.show()

    return img_tensor

#predict an image
IMAGE_PATH = '/home/phuc/Documents/code/AI/tensorflow course/transfer_learning/cats_and_dogs_filtered/train/dogs/dog.21.jpg'
new_image = load_image(IMAGE_PATH)

# check prediction
pred = model.predict(new_image)
# Positive numbers predict class 1, negative numbers predict class 0.
class_name = ['cat', 'dog']
class_predict = class_name[0] if pred < 0 else class_name[1]
print(class_predict)

#predit some images

