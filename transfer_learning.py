import os
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.callbacks import ModelCheckpoint, EarlyStopping

#get path
PATH = r'../datasets/cats_and_dogs_filtered'

train_dir = os.path.join(PATH, 'train')
val_dir = os.path.join(PATH, 'validation')

train_cats_dir = os.path.join(train_dir,'cats')
train_dogs_dir = os.path.join(train_dir,'dogs')
val_cats_dir = os.path.join(val_dir,'cats')
val_dogs_dir = os.path.join(val_dir,'dogs')

#get number of len dataset
num_cats_tr = len(os.listdir(train_cats_dir))
num_dogs_tr = len(os.listdir(train_dogs_dir))
num_cats_val = len(os.listdir(val_cats_dir))
num_dogs_val = len(os.listdir(val_dogs_dir))

total_train = num_cats_tr + num_dogs_tr
total_val = num_cats_val + num_dogs_val

#config
batch_size = 64
epochs = 20
IMG_HEIGHT = 160
IMG_WIDTH = 160

#3.generator data for train
image_gen_val = ImageDataGenerator(rescale=1./255)

#data aumentation with imagedataGenerator, only for train data
image_gen_train = ImageDataGenerator(
                    rescale=1./255,
                    rotation_range=45,
                    width_shift_range=.15,
                    height_shift_range=.15,
                    horizontal_flip=True,
                    zoom_range=0.5
                    )

#if have some classes, please change class_mode='categorical'
train_data_gen = image_gen_train.flow_from_directory(batch_size=batch_size,
                                                       directory=train_dir, #path to train data
                                                       shuffle=True,
                                                       target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                       class_mode='binary')


val_data_gen = image_gen_val.flow_from_directory(batch_size=batch_size,
                                                      directory=val_dir, #path ti validation data
                                                      target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                      class_mode='binary')

#get data and label
sample_training_image, label_test = next(train_data_gen)
# print(label_test)
# print(type(label_test))

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5, 5, i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(sample_training_image[i], cmap=plt.cm.binary)
    plt.xlabel(str(label_test[i]))

plt.show()

#import pretrained model
IMG_SIZE = 160
IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 3)

# Create the base model from the pre-trained model MobileNet V2
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                               include_top=False,
                                               weights='imagenet')

base_model.trainable = False
base_model.summary()

#add global average pooling and neural network
model = tf.keras.Sequential([
    base_model,
    tf.keras.layers.GlobalAveragePooling2D(),
    Dense(128, activation='relu'),
    Dropout(0.4),
    Dense(64, activation='relu'),
    Dense(1)
])

#change loss + metrics phu hop voi du an
# model.compile(optimizer='adam',
#               loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
#               metrics=['accuracy'])

model.compile(optimizer='adam',
              loss=tf.losses.BinaryCrossentropy(from_logits=True),
              metrics=[tf.metrics.BinaryAccuracy(threshold=0.0, name='accuracy')])

model.summary()


#checkpoint best model
checkpoint_filepath = 'checkpoint'
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    save_weights_only=True,
    monitor='val_acc',
    mode='max',
    save_best_only=True)

#early stop when converging
es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=3)

#train
history = model.fit_generator(train_data_gen,
                              epochs=50,
                              validation_data=val_data_gen,
                              steps_per_epoch=total_train // batch_size,
                              callbacks=[model_checkpoint_callback,es])


initial_epochs = 10
validation_steps = 20
print("[info] evaluate model")

#Evaluate the model
# plt.plot(history.history['accuracy'], label='accuracy')
# plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
# plt.xlabel('Epoch')
# plt.ylabel('Accuracy')
# plt.ylim([0.5, 1])
# plt.legend(loc='lower right')
# test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)

loss0, accuracy0 = model.evaluate(val_data_gen)

model.save("model.h5")




